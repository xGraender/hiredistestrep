CC = gcc

OPTIMIZATION?=-O3
WARNINGS=-Wall -W -Wstrict-prototypes -Wwrite-strings -Wno-missing-field-initializers
DEBUG_FLAGS?= -g -ggdb
REAL_CFLAGS=$(OPTIMIZATION) -fPIC $(WARNINGS) $(DEBUG_FLAGS)
REAL_LDFLAGS=$(LDFLAGS)

# JSON_C_DIR=./json-c
# CFLAGS = -I$(JSON_C_DIR) -I$(JSON_C_DIR)/build 
# LDFLAGS= -L$(JSON_C_DIR)/build

default: hiredis-example-libev 
#parse-json

hiredis-example-libev: src/example-libev.c hiredis/adapters/libev.h hiredis/libhiredis.a
	$(CC) -o ./$@ $(REAL_CFLAGS) -I./hiredis $< -lev hiredis/libhiredis.a $(REAL_LDFLAGS)

# parse-json: 
	# $(CC) $(LDFLAGS) $(CFLAGS) -o parse-json -c src/parse-json.c -ljson-c