# Сборка ---------------------------------------

# В качестве базового образа для сборки используем gcc:latest
FROM gcc:latest as build

COPY . /usr/src/app

# Установим рабочую директорию для сборки
WORKDIR /usr/src/app/hiredis

RUN apt-get update && \
    apt-get install -y \
      libev-dev \
    && \
    make static
	
WORKDIR /usr/src/app

RUN make


# Запуск ---------------------------------------

# В качестве базового образа используем ubuntu:latest
FROM ubuntu:latest

# Временное добавление библиотеки
RUN apt-get update && \
    apt-get install -y \
      libev-dev

# Установим рабочую директорию нашего приложения
WORKDIR /usr/src/app/

# Скопируем приложение со сборочного контейнера в рабочую директорию
COPY --from=build /usr/src/app .

# Установим точку входа
ENTRYPOINT ["./hiredis-example-libev", "teststring"]