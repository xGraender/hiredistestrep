#include<stdio.h>
#include<json.h>

int main(int argc, char **argv) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *hostname;
	struct json_object *port;

	size_t i;	

	fp = fopen("conf.json","r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "hostname", &hostname);
	json_object_object_get_ex(parsed_json, "port", &port);

	printf("hostname: %s\n", json_object_get_string(hostname));
	printf("port: %d\n", json_object_get_int(port));

}