#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <hiredis.h>
#include <async.h>
#include <adapters/libev.h>

void getCallback(redisAsyncContext *c, void *r, void *privdata) {
    redisReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* Disconnect after receiving the reply to GET */
    redisAsyncDisconnect(c);
}

void connectCallback(const redisAsyncContext *c, int status) {
    if (status != REDIS_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Connected...\n");
}

void disconnectCallback(const redisAsyncContext *c, int status) {
    if (status != REDIS_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Disconnected...\n");
}

void alarm_handler (int signum)
{
    printf ("Five seconds passed!\n");
}

int main (int argc, char **argv) {
    signal(SIGPIPE, SIG_IGN);
	signal(SIGALRM, alarm_handler);

	struct itimerval timer;
	timer.it_value.tv_sec = 0;
	timer.it_value.tv_usec = 250;
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = 0;
	
	setitimer(ITIMER_REAL, &timer, NULL);
	
	clock_t begin = clock();
    redisAsyncContext *c = redisAsyncConnect("redis", 6379);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error main: %s\n", c->errstr);
        return 1;
    }
	clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("redisAsyncConnect function time: %f\n", time_spent);
	
    redisLibevAttach(EV_DEFAULT_ c);
    redisAsyncSetConnectCallback(c,connectCallback);
    redisAsyncSetDisconnectCallback(c,disconnectCallback);
	begin = clock();
    redisAsyncCommand(c, NULL, NULL, "SET key %b", argv[argc-1], strlen(argv[argc-1]));
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("SET key function time: %f\n", time_spent);
	
	begin = clock();
    redisAsyncCommand(c, getCallback, (char*)"end-1", "GET key");
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("GET key function time: %f\n", time_spent);
	
	begin = clock();
    redisAsyncCommand(c, getCallback, (char*)"end-1", "XADD somestream * key %b", argv[argc-1], strlen(argv[argc-1]));
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("XADD somestream function time: %f\n", time_spent);
	
	sleep(1);
	ev_loop(EV_DEFAULT_ 0);
    return 0;
}


